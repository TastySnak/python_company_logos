
#-----Statement of Authorship----------------------------------------#
#
#  This is an individual assessment item.  By submitting this
#  code I agree that it represents my own work.  I am aware of
#  the University rule that a student must not act in a manner
#  which constitutes academic dishonesty as stated and explained
#  in QUT's Manual of Policies and Procedures, Section C/5.3
#  "Academic Integrity" and Section E/2.1 "Student Code of Conduct".
#
#    Student no: n9408410
#    Student name: Gary Murphy
#
#  NB: Files submitted without a completed copy of this statement
#  will not be marked.  All files submitted will be subjected to
#  software plagiarism analysis using the MoSS system
#  (http://theory.stanford.edu/~aiken/moss/).
#
#--------------------------------------------------------------------#



#-----Task Description-----------------------------------------------#
#
#  BUBBLE CHARTS
#
#  This task tests your skills at defining functions, processing
#  data stored in lists and performing the arithmetic calculations
#  necessary to display a complex visual image.  The incomplete
#  Python script below is missing a crucial function,
#  "draw_bubble_chart".  You are required to complete this function
#  so that when the program is run it produces a bubble chart,
#  using data stored in a list to determine the positions and
#  sizes of the icons.  See the instruction sheet accompanying this
#  file for full details.
#
#--------------------------------------------------------------------#  



#-----Preamble and Test Data-----------------------------------------#
#
#

# Module provided
#
# You may use only the turtle graphics functions for this task;
# you may not import any other modules or files.

from turtle import *


# Given constants
#
# These constant values are used in the main program that sets up
# the drawing window; do not change any of these values.

max_value = 350 # maximum positive or negative value on the chart
margin = 25 # size of the margin around the chart
legend_width = 400 # space on either side of the window for the legend
window_height = (max_value + margin) * 2 # pixels
window_width = (max_value + margin) * 2 + legend_width # pixels
font_size = 12 # size of characters on the labels in the grid
grid_size = 50 # gradations for the x and y scales shown on the screen
tick_size = 5 # size of the ticks on either side of the axes, in pixels


# Test data
#
# These are the data sets that you will use to test your code.
# Each of the data sets is a list containing the specifications
# for several icons ("bubbles") to display on the screen.  Each
# such list element specifies one icon using four values:
#
#    [icon_style, x_value, y_value, z_value]
#
# The 'icon_style' is a character string specifying which icon to
# to display.  Possible icons are named 'Icon 0' to 'Icon 4'.
# The final three values are integers specifying the icon's values
# in three dimensions, x, y and z.  The x and y values will be in
# the range -350 to 350, and determine where to place the icon on
# the screen.  The z value is in the range 0 to 350, and determines
# how big the icon must be (i.e., its widest and/or highest size on
# the screen, in pixels).

# The first icon in three different sizes
data_set_00 = [['Icon 0', -200, 200, 20],
               ['Icon 0', 200, 200, 120],
               ['Icon 0', 0, 0, 100]]

# The second icon in four different sizes
data_set_01 = [['Icon 1', -200, 200, 20],
               ['Icon 1', 200, 200, 120],
               ['Icon 1', 200, -200, 60],
               ['Icon 1', 0, 0, 100]]

# The third icon in five different sizes
data_set_02 = [['Icon 3', -200, 200, 300],     # 'Icon 2'
               ['Icon 3', -200, -200, 30],
               ['Icon 3', 200, -200, 90],
               ['Icon 3 ', 200, 200, 120],
               ['Icon 3', 0, 0, 100]]

# The fourth icon in four different sizes
data_set_03 = [['Icon 3', -200, 200, 300],
               ['Icon 3', -200, -200, 30],
               ['Icon 3', 200, -200, 90],
               ['Icon 3', 0, 0, 100]]

# The fifth icon in four different sizes
data_set_04 = [['Icon 4', 200, 200, 190],
               ['Icon 4', -200, -200, 10],
               ['Icon 4', 200, -200, 90],
               ['Icon 4', 0, 0, 100]]

# The next group of data sets test all five of your icons
# at the same time

# All five icons at the same large size
data_set_05 = [['Icon 0', -200, 200, 200],
               ['Icon 1', 200, 200, 200],
               ['Icon 2', 200, -200, 200],
               ['Icon 3', -200, -200, 200],
               ['Icon 4', 0, 0, 200]]

# All five icons at another size, listed in a different order
data_set_06 = [['Icon 4', 0, 0, 150],
               ['Icon 3', -200, -200, 150],
               ['Icon 2', -200, 200, 150],
               ['Icon 1', 200, 200, 150],
               ['Icon 0', 200, -200, 150]]

# All five icons arranged diagonally, at increasing sizes
data_set_07 = [['Icon 0', -200, -200, 15],
               ['Icon 1', -100, -100, 50],
               ['Icon 2', 0, 0, 100],
               ['Icon 3', 100, 100, 120],
               ['Icon 4', 200, 200, 180]]

# An extreme test in which all five icons are VERY small
data_set_08 = [['Icon 0', -100, -80, 5],
               ['Icon 2', 100, -100, 1],
               ['Icon 3', 10, 30, 2],
               ['Icon 1', 100, 100, 0],
               ['Icon 4', 200, 200, 4]]

# The next group of data sets are intended as "realistic" ones
# in which all five icons appear once each at various sizes in
# different quadrants in the chart

# Data occurs in all four quadrants
data_set_09 = [['Icon 0', -265, -80, 50],
               ['Icon 2', 100, -146, 78],
               ['Icon 3', -50, 130, 69],
               ['Icon 1', 210, 100, 96],
               ['Icon 4', 200, 300, 45]]

# All data appears in the top quadrants
data_set_10 = [['Icon 4', -265, 80, 140],
               ['Icon 2', 100, 146, 24],
               ['Icon 1', 10, 30, 99],
               ['Icon 0', 210, 100, 75],
               ['Icon 3', 200, 300, 65]]

# All data appears in the top right quadrant
data_set_11 = [['Icon 3', 265, 80, 140],
               ['Icon 1', 100, 146, 24],
               ['Icon 2', 20, 30, 109],
               ['Icon 4', 210, 205, 75],
               ['Icon 0', 200, 300, 65]]

# All data appears in the bottom left quadrant
data_set_12 = [['Icon 2', -265, -110, 130],
               ['Icon 3', -100, -146, 34],
               ['Icon 0', -25, -40, 73],
               ['Icon 1', -210, -200, 75],
               ['Icon 4', -180, -320, 65]]

# Another case where data appears in all four quadrants
data_set_13 = [['Icon 4', -265, 80, 96],
               ['Icon 3', 100, -46, 54],
               ['Icon 2', 50, 30, 89],
               ['Icon 1', -210, -190, 75],
               ['Icon 0', 250, 300, 123]]

# Yet another set with data in all four quadrants
data_set_14 = [['Icon 1', 212, -165, 90],
               ['Icon 2', 153, -22, 125],
               ['Icon 3', 84, 208, 124],
               ['Icon 4', -105, -58, 85],
               ['Icon 0', -62, 274, 57]]

# A random test - it produces a different data set each time you run
# your program!
from random import randint
max_rand_coord = 300
min_size, max_size = 20, 200

data_set_15 = [[icon,
                randint(-max_rand_coord, max_rand_coord),
                randint(-max_rand_coord, max_rand_coord),
                randint(min_size, max_size)]
               for icon in ['Icon 0', 'Icon 1', 'Icon 2', 'Icon 3', 'Icon 4']]

# Finally, just for fun, a random test that produces a montage
# by plotting each icon twenty times (which obviously doesn't
# make sense as a real data set)
data_set_16 = [[icon,
                randint(-max_rand_coord, max_rand_coord),
                randint(-max_rand_coord, max_rand_coord),
                randint(min_size, max_size)]
               for icon in ['Icon 0', 'Icon 1', 'Icon 2', 'Icon 3', 'Icon 4'] * 20]

#***** If you want to create your own test data sets put them here

data_set_17 = [['Icon 0', -250, -200, 300],
               ['Icon 1', 250, -200, 300],
               ['Icon 2', 150, 125, 300],
               ['Icon 3', 0, 325, 300],
               ['Icon 4', -150, 125, 300]]

data_set_18 = [['Icon 3', -200, 200, 200],
               ['Icon 3', 200, 200, 150],
               ['Icon 3', 0, 0, 350]]


#-----Student's Solution---------------------------------------------#
#

def draw_zip2(logo_height):
    penup()
    setheading(90)
    fd(logo_height / 14)

    circle_centre = pos()

    # Draw circles
    fd((logo_height / 7) * 3)      # Outer circle is approximately 6/7 the height of the logo.

    top_point = pos()

    left(90)
    pencolor("#000000")            # Black colour.
    fillcolor("#000000")
    pendown()
    begin_fill()
    circle((logo_height / 7) * 3)
    end_fill()
    penup()
    goto(circle_centre)
    setheading(90)
    fd((logo_height / 7.8) * 3)    # Inner circle is slightly smaller than the other.
    left(90)
    pencolor("#FFFFFF")            # White colour.
    fillcolor("#FFFFFF")
    pendown()
    begin_fill()
    circle((logo_height / 7.8) * 3)
    end_fill()
    penup()
    goto(circle_centre)

    # Draw body
    pencolor("black")
    fillcolor("black")
    setheading(90)
    pendown()
    begin_fill()
    circle(logo_height / 8, 90)

    top_arm = pos()                # For other arm to measure against.

    circle(logo_height / 8, 90)
    fd(logo_height / 40)
    left(90)
    fd(logo_height / 16)
    left(90)
    fd(logo_height / 40)
    setheading(70)
    circle(-logo_height / 16, 100)
    setheading(240)
    fd(logo_height / 3.5)
    setheading(-10)
    fd(logo_height / 10)
    setheading(240)
    while ycor() > (top_point[1] - logo_height):       # Stop at bottom of logo.
        fd(1)
    setheading(45)
    fd(logo_height / 1.7)
    setheading(180)
    fd(logo_height / 14)
    setheading(45)
    fd(logo_height / 14)
    setheading(0)
    fd(logo_height / 14)
    circle(logo_height / 6, 90)
    setheading(90)
    while ycor() < top_arm[1]:              # Keeps arms to same height.
        fd(1)
    left(90)
    fd(logo_height / 16)

    head_yref = pos()               # Use to help position head.

    left(90)
    fd(logo_height / 40)
    circle(-logo_height / 8, 90)

    head_xref = pos()               # Use to help position head.

    circle(-logo_height / 8, 60)
    circle(logo_height / 8, 20)
    end_fill()
    penup()
    goto(head_xref[0], head_yref[1])

    # Draw head
    setheading(225)
    fd(logo_height / 10)

    head_start = pos()                 # Store the return point.

    left(70)
    pendown()
    begin_fill()
    circle(logo_height / 12, 180)
    setheading(130)
    fd(logo_height / 3.8)
    left(160)
    fd(logo_height / 7)
    right(150)
    fd(logo_height / 10)
    left(150)
    fd(logo_height / 7)
    right(140)
    fd(logo_height / 20)
    goto(head_start)
    end_fill()
    penup()

def draw_paypal(logo_height):

    # Get useful coords.
    penup()
    setheading(-90)
    fd(logo_height / 2 - logo_height / 17.5)

    lower_p_start = pos()

    circle(-logo_height / 17.5, 80)

    lower_p_end = pos()
    lower_p_end_heading = heading()

    # Draw Lower P
    goto(lower_p_start)
    fillcolor("#0099FF")              # Light Blue colour.
    pencolor("#0099FF")
    pendown()
    begin_fill()

    setheading(80)
    fd(logo_height / 4)
    circle(-logo_height / 17.5, 80)
    fd(logo_height / 9)
    circle(logo_height / 5, 180)
    fd(logo_height / 4)

    top_lower_p = pos()                # Needed for when drawing the "shape"

    circle(logo_height / 17.5, 80)
    while ycor() > lower_p_start[1]:
        fd(1)

    ref_for_upper_p_start = pos()

    while ycor() > lower_p_end[1]:          # Keeps from drawing too far down
        circle(logo_height / 20.75, 1)
    setheading(lower_p_end_heading - 180)
    goto(lower_p_end)
    circle(logo_height / 17.5, 80)
    end_fill()

    # Get useful coords.
    penup()
    goto(ref_for_upper_p_start)
    setheading(80)
    fd(logo_height / 14)

    upper_p_start = pos()

    left(90)
    circle(-logo_height / 17.5, 80)

    ref_for_upper_p_end = pos()

    goto(upper_p_start)
    setheading(260)
    circle(-logo_height / 17.5, 80)

    upper_p_end = pos()

    # Draw Upper P
    goto(upper_p_start)
    setheading(80)
    fillcolor("#003399")                  # Blue colour.
    pencolor("#003399")
    pendown()
    begin_fill()
    fd(logo_height / 4)                   # Measured against Lower P.
    fd(logo_height / 14)                  # Extend a bit further.
    circle(-logo_height / 17.5, 80)
    fd(logo_height / 7)
    circle(logo_height / 3.9, 180)
    fd(logo_height / 4)
    circle(logo_height / 17.5, 80)
    while ycor() > ref_for_upper_p_end[1]:     # Find position for bottom of P.
        fd(1)                                  # And draw curve to not extend too far down.
    while ycor() > upper_p_start[1]:
        circle(logo_height / 20.75, 1)
    goto(upper_p_start)
    end_fill()
    penup()
    setheading(80)
    fd(logo_height / 14)
    fd(logo_height / 4)

    # Draw inner shape
    shape_start = pos()

    pendown()
    pencolor("#000066")              # Darker Blue colour.
    fillcolor("#000066")
    begin_fill()
    circle(-logo_height / 17.5, 80)
    fd(logo_height / 7.0)
    circle(logo_height / 3.9, 84)
    left(60)
    last_ycor = ycor()               # Variable to make sure while loop stays finite using if statement.
    while ycor() < top_lower_p[1]:
        if ycor() < last_ycor:
            break
        last_ycor = ycor()
        circle(logo_height / 5.0, 1)
    setheading(180)
    goto(top_lower_p)
    circle(logo_height / 17.5, 80)
    goto(shape_start)
    end_fill()
    penup()

def draw_solar(logo_width):
    def symbol(radius):                 # Logo is made of the same shape being repeated.
        pendown()                       # Function accepts a variable to change its size.
        begin_fill()
        circle(radius, 90)
        turn_value = 3
        for line in xrange(4):             # Draws a curve to resemble shape of symbol.
            left(turn_value)
            fd(radius / 1.25)
            turn_value += 2
        goto(top_pos)
        penup()
        goto(base_pos)
        setheading(base_angle - 180)
        pendown()
        circle(-radius, 90)
        turn_value = 3
        for line in xrange(4):
            right(turn_value)
            fd(radius / 1.25)
            turn_value += 2
        goto(top_pos)
        end_fill()

    pencolor("#FF6600")                 # Orange colour.
    fillcolor("#FFFF00")                # Yellow colour.
    start_pos = pos()
    setheading(-85)                     # Offset logo to similar angle to original.
    penup()
    fd(logo_width / 2)                  # Position turtle on outside edge to draw outer ring of symbols.
    left(90)
    for number_of_symbols in xrange(15):
        penup()
        circle(logo_width / 2, 24)    # The first point of A

        top_angle = heading()       # Store position and facing for turtle to return to.
        top_pos = pos()

        left(90)
        fd(logo_width / 5)
        left(90)

        base_pos = pos()                             # Goto point and facing for function.
        base_angle = heading()
        sym_length = distance(top_pos)               # The multiplier is used to calculate the length
        multiplier = sym_length / (logo_width / 2)   # of the smaller symbol as it is drawn differently.

        symbol(logo_width / 22.5)
        setheading(top_angle)
    penup()

    goto(start_pos)
    setheading(-85)
    fd(logo_width / 3.8)                        # Position to draw inner ring of symbols.
    left(90)
    for number_of_symbols in xrange(15):
        penup()
        circle(logo_width / 3.8, 24)

        base_pos = pos()
        base_angle = heading()

        left(90)
        fd((logo_width / 4) * multiplier)
        top_pos = pos()
        goto(base_pos)
        setheading(base_angle)
        symbol(logo_width / 45)
        penup()
        goto(base_pos)
        setheading(base_angle)
    penup()

def draw_tesla(logo_height):
    fillcolor("#FF0000")                          # Red colour.
    pencolor("#FFFFFF")                           # White colour.
    start_pos = pos()

    # draw top part of logo
    penup()
    goto(start_pos[0], start_pos[1] + logo_height / 2)

    topOfLogo = pos()

    pendown()
    begin_fill()
    setheading(180)
    circle(logo_height, 20)

    topLeftTopLogo = pos()     # Far left point to calculate logo width and get letter positioning.

    setheading(290)
    fd(logo_height / 25)
    setheading(22.5)
    circle(-logo_height * 0.75, 25)
    setheading(0)
    while xcor() < start_pos[0]:    # Don't draw further than the middle.
        fd(1)
    pencolor("#FF0000")
    goto(start_pos[0], start_pos[1] + logo_height / 2)
    end_fill()
    setheading(0)
    begin_fill()
    pencolor("#FFFFFF")
    circle(-logo_height, 20)

    logo_width = distance(topLeftTopLogo)   # Get logo width to calculate letters and spacing.

    setheading(250)
    fd(logo_height / 25)
    setheading(157.5)
    circle(logo_height * 0.75, 25)
    setheading(180)
    while xcor() > start_pos[0]:                  # Don't draw further than the middle.
        fd(1)
    pencolor("#FF0000")
    goto(start_pos[0], start_pos[1] + logo_height / 2)
    end_fill()
    pencolor("#FFFFFF")

    # draw bottom part of logo
    penup()
    setheading(-90)
    fd(logo_height / 6)

    bottomLogoStart = pos()           # Store point to return to and draw other side

    pendown()
    begin_fill()
    setheading(130)
    forward(logo_height / 10)
    left(50)
    circle(logo_height * 0.75, 20)
    left(100)
    circle(logo_height * 0.2, 30)
    left(120)
    circle(logo_height * -0.05, 75)
    right(5)
    circle(logo_height * -0.5, 15)
    right(76)
    while xcor() < start_pos[0]:          # Don't draw further than the middle.
        fd(1)

    bottomLogoBottom = pos()        # Store goto point

    penup()
    goto(bottomLogoStart)
    pendown()
    setheading(50)
    forward(logo_height / 10)
    right(50)
    circle(-logo_height * 0.75, 20)
    right(100)
    circle(-logo_height * 0.2, 30)
    right(120)
    circle(-logo_height * -0.05, 75)
    left(5)
    circle(-logo_height * -0.5, 15)
    goto(bottomLogoBottom)
    end_fill()

    # Go to top left of T
    penup()
    goto(topLeftTopLogo[0], start_pos[1] - logo_height / 2)

    bottomLeftLogo = pos()     # Store coords of bottom part of logo

    setheading(90)
    fd(logo_height / 7.5)

    letter_height = distance(topLeftTopLogo[0], start_pos[1] - logo_height / 2)   # Calculate letter height

    setheading(0)

    # Draw T
    pendown()
    begin_fill()

    letter_width = logo_width / 5.5                     # Calculate letter and space width
    space_width = (logo_width - letter_width * 5) / 4

    fd(letter_width)
    right(90)

    letterLineWidth = letter_width / 5    # Thickness of the lettering

    circle(-letter_width / 5, 90)
    fd((letter_width - (letter_width / 5) * 3) / 2)
    left(90)
    loop = 0                              # Variable to store height of side
    while ycor() > bottomLeftLogo[1]:
        fd(1)
        loop += 1
    right(90)
    fd(letter_width / 5)
    right(90)
    fd(loop)
    left(90)
    fd((letter_width - (letter_width / 5) * 3) / 2)
    circle(-letter_width / 5, 90)
    end_fill()

    # Go to position E
    penup()
    right(90)
    fd(letter_width + space_width)

    # draw E
    def top_of_E():          # The letter is made of the same shape repeated 3 times.
        pendown()            # These functions draw the stroke and move the turtle.
        begin_fill()
        fd(letter_width)
        right(90)
        circle(-letter_width / 5, 90)
        fd(letter_width - 2 * (letter_width / 5))
        circle(-letter_width / 5, 90)
        end_fill()

    def mov_E():
        penup()
        setheading(-90)
        fd(letterLineWidth + letter_space)
        left(90)

    letter_space = (letter_height - 3 * letterLineWidth) / 2     # Space between letter strokes.

    top_of_E()
    mov_E()
    top_of_E()
    mov_E()
    top_of_E()

    # Go to position S
    penup()
    fd(2 * letter_space + 2 * letterLineWidth)
    right(90)
    fd(letter_width + space_width)
    right(90)

    # Draw S
    pendown()
    begin_fill()
    for half_S in xrange(2):                                # Measurements are exactly the same for each half of S.
        fd(letter_height / 2 + letterLineWidth / 2)
        left(90)
        fd(letter_width - letter_width / 5)
        right(90)
        fd((letter_height - (letter_width / 5) * 3) / 2)
        right(90)
        fd(letter_width - 2 * (letter_width / 5))
        circle(letter_width / 5, 90)
        left(90)
        fd(letter_width)
        left(90)
    end_fill()

    # Go to position L
    penup()
    setheading(0)
    fd(letter_width + space_width)

    # Draw L
    pendown()
    begin_fill()
    fd(letterLineWidth)
    right(90)
    fd(letter_height - letterLineWidth)
    left(90)
    fd(letter_width - letterLineWidth)
    right(90)
    circle(-letter_width / 5, 90)
    fd(letter_width - letterLineWidth)
    right(90)
    fd(letter_height)
    end_fill()

    # Go to position A
    penup()
    setheading(0)
    fd(letter_width + space_width)

    # Draw A
    top_of_E()                              # Identical part of letter E drawn
    penup()
    setheading(-90)
    fd(((letter_height - (letter_width / 5) * 3) / 2) + letterLineWidth)
    pendown()
    begin_fill()
    fd(letter_height / 2 + letterLineWidth / 2)
    left(90)
    fd(letterLineWidth)
    left(90)
    fd(letter_height / 2 - letterLineWidth / 2)
    right(90)
    fd(letter_width - 2 * letterLineWidth)
    right(90)
    fd(letter_height / 2 - letterLineWidth / 2)
    left(90)
    fd(letterLineWidth)
    left(90)
    fd(letter_height / 2 + letterLineWidth / 2)
    left(90)
    fd(letter_width)
    end_fill()
    penup()

def draw_spaceX(logo_width):
    fillcolor("#003366")                      # Blue colour.
    pencolor("#003366")
    coordStart = (xcor(), ycor())             # Useful when calculating the X symbol.

    # Edge multipliers in terms of logo width
    corner = logo_width / (100 / 2.0)
    side = logo_width / (100 / 3.0)
    outside_edge = logo_width / (100 / 6.0)
    inside_edge = logo_width / (100 / 8.0)

    # Go to start of S
    penup()
    setheading(180)
    forward(logo_width/2)

    # Draw S
    pendown()
    begin_fill()
    right(90)
    circle(-corner, 90)

    top_y = ycor()                            # Useful when calculating the X symbol.

    forward(outside_edge)
    circle(-corner, 90)
    right(90)
    forward(inside_edge)
    left(90)
    forward(side)
    left(90)
    forward(outside_edge)
    circle(-corner, 90)
    forward(side)
    circle(-corner, 90)

    bottom_y = ycor()              # Store bottom most Y coordinate

    forward(outside_edge)
    circle(-corner, 90)
    right(90)
    forward(inside_edge)
    left(90)
    forward(side)
    left(90)
    forward(outside_edge)
    circle(-corner, 90)
    forward(side)
    end_fill()

    # Go to start of P
    penup()
    circle(-corner, 90)
    forward(outside_edge + (corner *3))

    #Draw P
    pendown()
    begin_fill()
    forward(inside_edge)
    circle(-corner, 90)
    forward(side)
    circle(-corner, 90)
    forward(outside_edge)
    left(90)
    forward(side + corner)
    right(90)
    forward(corner)
    right(90)
    forward(2 * outside_edge)
    end_fill()

    # Position for inside of P
    penup()
    right(180)
    forward(corner)
    left(90)
    forward(corner)

    # Draw inside of P
    pendown()
    fillcolor("Grey")
    begin_fill()
    for half_of_hole in xrange(2):
        forward(outside_edge)
        right(90)
        forward(side)
        right(90)
    end_fill()

    # Position for A
    penup()
    forward(outside_edge + (corner * 3))
    a_startPos = (xcor(), ycor())

    # Draw A
    pendown()
    fillcolor("#003366")
    begin_fill()
    left(60)
    forward(corner)
    right(120)
    while ycor() > bottom_y:
        forward(1)
    right(120)
    forward(corner)
    right(60)
    forward(side)
    left(60)
    forward(inside_edge)
    right(120)
    forward(corner)
    right(60)
    forward(outside_edge)
    goto(a_startPos)
    end_fill()

    # Position for C
    penup()
    setheading(0)
    forward(5 * corner)
    right(90)

    # Draw C
    pendown()
    begin_fill()
    forward(2 * side + corner)
    circle(corner, 90)
    forward(outside_edge)
    circle(corner, 90)
    left(90)
    forward(inside_edge)
    right(90)
    forward(2 * side + corner)
    right(90)
    forward(inside_edge)
    left(90)
    circle(corner, 90)
    forward(outside_edge)
    circle(corner, 90)
    end_fill()

    # Position for E
    penup()
    setheading(0)
    forward(4 * corner + outside_edge)

    # Draw E
    pendown()
    begin_fill()
    for half_of_stroke in xrange(2):                 # Top part of E.
        fd(outside_edge + 2 * corner)
        left(90)
        fd(corner)
        left(90)
    setheading(-90)
    penup()
    fd(corner)

    coordE = (xcor(), ycor())

    pendown()                          # Bottom part of E.
    while ycor() > bottom_y:
        fd(1)
    left(90)
    fd(outside_edge + 2 * corner)
    left(90)
    fd(corner)
    left(90)
    fd(inside_edge)
    right(90)
    fd(2 * corner)
    right(90)
    fd(outside_edge)
    left(90)
    while ycor() < coordE[1]:
        fd(1)
    goto(coordE[0], coordE[1])
    end_fill()

    # Position for X
    penup()
    left(180)
    while ycor() > bottom_y:
        fd(1)
    left(90)
    fd(outside_edge + 4 * corner)
    x_coordStart = (xcor(), ycor())        # Store starting point.

    # Draw X
    fillcolor("#9999ff")                   # Light Blue colour.
    pencolor("#9999ff")
    pendown()
    begin_fill()                           # Draws long shape of X.
    left(45)
    ang = 1
    mov = logo_width / 100.0
    while xcor() < coordStart[0] + logo_width / 2.0:
        for still_drawing in xrange(int(ceil(abs(mov)))):     # Value in mov must be made positive, if negative.
            fd(1)                                             # Then rounded up and turned into an integer for xrange.
        right(ang)
        mov -= 1.0 / logo_width
        ang += 0.001
    goto(coordStart[0] + logo_width / 2.0, ycor())
    right(175)
    while ycor() > bottom_y:
        for still_drawing in xrange(int(ceil(abs(mov)))):
            fd(1)
            if ycor() <= bottom_y:                    # Makes sure draw doesn't extend too far down.
                break
        left(ang)
        mov += 1.0 / logo_width
        ang += 0.001
    setheading(180)
    while xcor() > x_coordStart[0]:
        fd(1)
    end_fill()
    setheading(90)
    penup()
    fd(2 * side + 3 * corner)
    setheading(-45)
    fillcolor("#003366")
    pencolor("#003366")
    pendown()
    begin_fill()

    topXCoord = (xcor(), ycor())

    fd(4 * corner)                      # Draw upper part of X.
    left(80)
    fd(side)
    left(100)
    while ycor() < top_y:
        fd(1)
    setheading(180)

    topXLength = 0

    while xcor() > topXCoord[0]:
        fd(1)
        topXLength += 1
    end_fill()
    penup()
    setheading(-45)
    fd(6.5 * corner)
    pendown()
    begin_fill()

    bottomXStart = (xcor(), ycor())        # Start position of lower part of X.

    setheading(40)                         # Draw lower part of X.
    fd(side)
    setheading(-45)
    while ycor() > bottom_y:
        fd(1)
    setheading(180)
    fd(topXLength)
    goto(bottomXStart)
    end_fill()
    penup()


def draw_legend():
    penup()
    goto((window_width / 2), (window_height / 2))
    setheading(180)
    fillcolor('Grey')
    pencolor("#000066")              # Dark Blue colour.
    pendown()
    begin_fill()
    for half_border in xrange(2):
        forward(200)
        left(90)
        forward(window_height)
        left(90)
    end_fill()
    penup()
    color("#FFFFFF")                         # White colour.
    goto((window_width / 2) - 100, 350)
    write("Companies", True, align="center", font=("Arial", 15, "normal"))
    goto((window_width / 2) - 100, 330)
    write("related to", True, align="center", font=("Arial", 15, "normal"))
    goto((window_width / 2) - 100, 310)
    write("Elon Musk", True, align="center", font=("Arial", 15, "normal"))

    goto((window_width / 2) - 100, 280)
    draw_spaceX(110)
    goto((window_width / 2) - 100, 250)
    color("#000000")                           # Black colour.
    write("Space X", True, align="center", font=("Arial", 10, "normal"))

    goto((window_width / 2) - 100, 175)
    draw_tesla(110)
    goto((window_width / 2) - 100, 100)
    color("#000000")
    write("Tesla Motors", True, align="center", font=("Arial", 10, "normal"))

    goto((window_width / 2) - 100, 20)
    draw_solar(110)
    goto((window_width / 2) - 100, -55)
    color("#000000")
    write("Solar City", True, align="center", font=("Arial", 10, "normal"))

    goto((window_width / 2) - 100, -130)
    draw_paypal(110)
    goto((window_width / 2) - 100, -205)
    color("#000000")
    write("Pay Pal", True, align="center", font=("Arial", 10, "normal"))

    goto((window_width / 2) - 100, -290)
    draw_zip2(110)
    goto((window_width / 2) - 100, -345)
    color("#000000")
    write("Zip 2", True, align="center", font=("Arial", 10, "normal"))

def draw_bubble_chart(dummy_parameter_list):
    for i in dummy_parameter_list:
        goto(i[1], i[2])
        if i[0] == 'Icon 0':
            draw_zip2(i[3])
        elif i[0] == 'Icon 1':
            draw_paypal(i[3])
        elif i[0] == 'Icon 2':
            draw_solar(i[3])
        elif i[0] == 'Icon 3':
            draw_spaceX(i[3])
        elif i[0] == 'Icon 4':
            draw_tesla(i[3])

#--------------------------------------------------------------------#



#-----Main Program---------------------------------------------------#
#
# This main program sets up the drawing environment, ready for you
# to start drawing your bubble chart.  Do not change any of
# this code except the lines marked '*****'
    
# Set up the drawing window with enough space for the grid and
# legend
setup(window_width, window_height)
title('Companies related to Elon Musk') #***** Choose a title appropriate to your icons

# Draw as quickly as possible by minimising animation
hideturtle()     #***** You may comment out this line while debugging
                 #***** your code, so that you can see the turtle move
speed('fastest') #***** You may want to slow the drawing speed
                 #***** while debugging your code

# Choose a neutral background colour                    
bgcolor('grey')
# Draw the two axes
pendown() # assume we're at home, facing east
forward(max_value)
left(180) # face west
forward(max_value * 2)
home()
setheading(90) # face north
forward(max_value)
left(180) # face south
forward(max_value * 2)
penup()

# Draw each of the tick marks and labels on the x axis
for x_coord in range(-max_value, max_value + 1, grid_size):
    if x_coord != 0: # don't label zero
        goto(x_coord, -tick_size)
        pendown()
        goto(x_coord, tick_size)
        penup()
        write(str(x_coord), align = 'center',
              font=('Arial', font_size, 'normal'))
        
# Draw each of the tick marks and labels on the y axis
for y_coord in range(-max_value, max_value + 1, grid_size):
    if y_coord != 0: # don't label zero
        goto(-tick_size, y_coord)
        pendown()
        goto(tick_size, y_coord)
        penup()
        goto(tick_size, y_coord - font_size / 2) # Allow for character height
        write('  ' + str(y_coord), align = 'left',
              font=('Arial', font_size, 'normal'))

# Call the student's function to display the data set
draw_bubble_chart(data_set_04) #***** Change this for different data sets
draw_legend()
    
# Exit gracefully
hideturtle()
done()

#
#--------------------------------------------------------------------#




